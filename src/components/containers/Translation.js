import React from 'react';
//import { Redirect, useHistory } from 'react-router-dom';
//import { BrowserRouter as Router } from 'react-router-dom';

 


class Translation extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        input: "",
        translation: null,
        inputArray: []
      };
    }
    /*
    this method allows for translation by taking in the user input, looping through the input and finding the image
    that matches the input
    */
    translate=() => {
        console.log(this.state.input);
        if(!this.state.input){
            return;        
            // goes through the array of input and finds the right image based on the input
        }else{
        const translation = Array.prototype.map.call(this.state.input, (char, index) => {
            if(!char.match(/[A-z]/))
                
                return (<span key={index}className="spacer">{char}</span>);            
                return (<img src={"/images/" + char.toLowerCase() + ".png"} alt={char} key={index}/>);
                
        });        
        
        console.log(this.state.translation, "se her");
        this.setState({
            translation: translation
        });
        console.log(this.state.input);
        console.log(this.state.translation);
        this.arrayUpdate();
    }
    }
    /*
    this method takes in the input and adds it to an array,
    if the amount of elements in the array become equal or great than 10 then
    it will delete the first element in the array. 
    this method is called from the translate method above, so that it is executed every time
    the user presses translate
    */
    arrayUpdate = ()=>{
        this.state.inputArray.push(this.state.input);
        if(this.state.inputArray.length>=10){
            this.state.inputArray.shift();
            console.log("the tenth element was deleted");
        }
        localStorage.setItem('translations', this.state.inputArray);
        
        console.log(this.state.inputArray);
    }
    //this sets the state input equal to the user input 
    myStateHandler = (event) =>{
        
        this.setState({ input: event.target.value });
        
    }
    
    //allows for navigation to the profile page
    toProfile = ()=>{
        window.location.href="/profile";
    }
    //allows for navigation to rune page
    toRunes = ()=>{
        window.location.href="/runes";
    }
    //this checks if there is a username, if there is no username it wil return to the start page
    checkUserName = () =>{
        if(localStorage.getItem("username")===null){
            window.location.href="/register";
            console.log("no username return to start and register yourself");
        }
    }
    render() {
      return (
        <div>
            {  this.checkUserName() }
            <h1>Translation</h1>
        <div>
            <div>
                <button onClick={this.toProfile}>To profile</button>
            </div>
            <div>
                <button onClick={this.toRunes}>To rune translator</button>
            </div>
            
        <input type="text" id="input" placeholder="translate a word or a sentence" value={this.state.input} onChange={this.myStateHandler}></input>
            <button onClick={ () => this.translate() }>Translate</button>
            <p>{ this.state.translate }</p>
            <div>
                { this.state.translation }
            </div>
            
        </div>

          
        </div>
      );
    }
  }
  export default Translation;