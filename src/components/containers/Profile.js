import React from 'react'; 

const Profile = () => {
    //deletes all the translations from localstorage, to remove them from the page I have made it also reload the page as it is executed
    const deleteTranslations = () =>{
        localStorage.removeItem("translations");
        window.location.reload();
    };
    //deletes the user and navigates to register page
    const deleteUser = () => {
        localStorage.clear();
        window.location.href="/register"
    }
    //checks if there is a user name, if no user name it returns to the register page
    const checkUserName = () =>{
        if(localStorage.getItem("username")===null){
            window.location.href="/register";
            console.log("no username return to start and register yourself");
        }
    }

    return (
        <div>
            { checkUserName() }
        <h1>profile</h1>
        <div>
            <button onClick={ deleteTranslations }>delete translation log</button>
            { localStorage.getItem("translations")}
        </div>
        <div>
            <button onClick={ deleteUser }>Delete user and log out</button>
        </div>
    </div>
    );
};
export default Profile;