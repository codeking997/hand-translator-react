import React from 'react';
//import { BrowserRouter as Router } from 'react-router-dom';

 
//this works the same way as translation, the only exception is that the translations are not stored in local storage

class Translation extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        input: "",
        translation: null
      };
    }
    translate=() => {
        console.log(this.state.input);
        if(!this.state.input){
            return;        
            // Output image for each character in input string
        }else{
        const translation = Array.prototype.map.call(this.state.input, (char, index) => {
            if(!char.match(/[A-z]/))
                
                return (<span key={index}className="spacer">{char}</span>);            
                return (<img src={"/runes/" + char.toLowerCase() + ".jpg"} alt={char} key={index}/>);
            
                
        });        
        //this.props.saveTranslation(this.state.input);        
        // Store the image elements in state, so the rendered result can update
        console.log(this.state.translation, "se her");
        this.setState({
            translation: translation
        });
        console.log(this.state.input);
        console.log(this.state.translation);
    }
    }
    myStateHandler = (event) =>{
        
        
        this.setState({ input: event.target.value });
        
    }
    
    toProfile = ()=>{
        window.location.href="/profile";
    }
    checkUserName = () =>{
        if(localStorage.getItem("username")===null){
            window.location.href="/register";
            console.log("no username return to start and register yourself");
        }
    }
    render() {
      return (
        <div>
            { this.checkUserName() }
            <h1>Translation</h1>
        <div>
            <div>
                <button onClick={this.toProfile}>To profile</button>
            </div>
            
        <input type="text" id="input" placeholder="translate a word or a sentence" value={this.state.input} onChange={this.myStateHandler}></input>
            <button onClick={ () => this.translate() }>Translate</button>
            <p>some rules here, because not all letters in the elder futhark have english equivilants you must be creative, use v instead w, k instead of c
                also not all elder futhark letters have been included because some of them are double letters like ng.
            </p>
            <div>
                { this.state.translation }
            </div>
        </div>

          
        </div>
      );
    }
  }
  export default Translation;