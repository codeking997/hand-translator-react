import React, { useState } from 'react';

const RegisterForm = props=>{

    const [ username, setUsername ] = useState('');
    //const [ password, setPassword ] = useState('');
    //const [registerUser, setRegisterError] = useState('');
    //when this is executed the username is stored and it moves to the translate page
    const onRegisterClicked = async ev => {
        console.log("EVENT: ", ev.target);

        props.complete(true);
        window.location.href="/translate";
        console.log(username);

    };
    //makes sure that the username is updated as the user types it in
    const onUserNameChanged = function(ev){ 
        setUsername(ev.target.value);
        localStorage.setItem('username', ev.target.value);
    }

    //const onPasswordChanged = ev => setPassword(ev.target.value);

    return (
        <form>
            <div>
                <p>register a username to use the translator</p>
                <label>
                    username: 
                </label>
                <input type="text" placeholder="Enter a username" onChange={ onUserNameChanged }/>
            </div>
            <div>
                <button type="button" onClick={ onRegisterClicked }>Register</button>
            </div>
        </form>
    );
}
export default RegisterForm;