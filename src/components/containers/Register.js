import React from 'react';
import { Redirect, useHistory } from 'react-router-dom';
import RegisterForm from './forms/RegisterForm'




    const Register = () => {

       
        //uses useHistory to navigate between different pages
        /*
        checks if there is a username and then allows to move to the translate page
        */
        const history = useHistory();
        const handleRegisterComplete = ( result) => {
            console.log('yes yiy are triggered', result);
            if(result){
                history.replace("/translate");
            }
        };

        
        //has a redirect that if there is a username stored in local storage it will automatically redirect to translate
        return (
            <div>
                { localStorage.getItem("username")!==null && <Redirect to="/translate"/> }
            <h1>Register your account at the translator</h1>
            <RegisterForm complete={ handleRegisterComplete }/>
        </div>
        );
    };

export default Register;
