import React from 'react';
import './App.css';
import Register from './components/containers/Register';
import Translation from './components/containers/Translation'
import Profile from './components/containers/Profile'
import Runes from './components/containers/Runes'
import { BrowserRouter as Router, Switch, Route } from 'react-router-dom';

function App() {
  return (
    <Router>
      <div className="App">
        <Switch>
        <Route path="/register" component={ Register }/>
        <Route path="/translate" component={ Translation }/>
        <Route path="/profile" component={ Profile }/>
        <Route path="/runes" component={ Runes }/>
        </Switch>
      
    </div>
    </Router>
    
  );
}

export default App;
